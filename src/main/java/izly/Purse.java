package izly;

import org.mockito.Mockito;
import secretCode.CodeSecret;

public class Purse {

    private Purse() {
    }

    public static Purse createPurse(double plafond, int nbOperationMax, CodeSecret codeSecret) throws CreationPurseException, RejetTransactionException {
        Purse purse = Mockito.mock(Purse.class);
        try {
            Mockito.doThrow(new RejetTransactionException()).when(purse).debite(Mockito.anyDouble(), Mockito.anyString());
            Mockito.doNothing().when(purse).debite(Mockito.anyDouble(), Mockito.eq("9876"));
        } catch (RejetTransactionException e) {
            e.printStackTrace();
        }
        Mockito.when(purse.getSolde()).thenReturn(50.0, 0.0);
        return purse;
    }

    public double getSolde() {
        return 0;
    }

    public void debite(double montant, String codeProposé) throws RejetTransactionException {
    }

    public void credite(double montant) throws RejetTransactionException {
    }

}
